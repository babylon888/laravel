<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = ['alias'];
    public function cate()
    {
        return $this->belongsTo(Cate::class);
    }
}

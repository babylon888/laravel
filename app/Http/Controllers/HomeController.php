<?php

namespace App\Http\Controllers;

use App\Cate;
use App\Product;
use Illuminate\Http\Request;

use App\Http\Requests;

class HomeController extends Controller
{
    public function index()
    {
        $cates = Cate::where('parent_id',0)->get();

        $products = Product::limit(27)->orderBy('id','desc')->get();

        return view('frontend.index',compact('cates','products'));
    }

    public function cate($alias)
    {

        $cates = Cate::where('parent_id',0)->get();
        $cate = Cate::where('alias', $alias)->first();


        $products = Product::where('cate_id',$cate->id)->get();


        return view('frontend.cate',compact('cates','products','cate'));
    }

    public function detail($cate_alias,$product_alias,$product_id)
    {
        $cates = Cate::where('parent_id',0)->get();
        $product = Product::where('id', $product_id)->first();
        return view('frontend.detail', compact('product','cates'));

    }

    public function search()
    {

        $cates = Cate::where('parent_id',0)->get();
        $products = new Product();

        if (!empty($_GET['cate_id'])) {

            $products = $products->where('cate_id', $_GET['cate_id']);
        }
        if (!empty($_GET['name'])) {

            $products = $products->where('title','like', "%".$_GET['name']."%");
        }
        if(!empty($_GET['price'])){
            $price = $_GET['price'];
            $price=explode('-',$price);
            $products = $products->where('price', '>=',$price[0])
                ->where('price','<',$price[1]);
        }



        if (!isset($_GET['cate_id']) && !isset($_GET['name']) && !isset($_GET['price'])) {
            $products = $products->all();
        }else{
            $products = $products->get();
        }

        return view('frontend.search', compact('products','cates'));
    }
}

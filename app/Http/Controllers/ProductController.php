<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductAddRequest;
use App\Product;
use App\Cate;

use Illuminate\Http\Request;

use App\Http\Requests;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate(10);

        return view('admin.product.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cates = Cate::all();


        return view('admin.product.create',compact('cates'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductAddRequest $request)
    {
        $data = $request->all();
        if ($request->hasFile('image')) {
            $file = $request->file('image');

            $file->move('uploads',$file->getClientOriginalName());
            $data['image'] = $file->getClientOriginalName();
        }

        Product::create($data);
        return redirect()->route('admin.product.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $cates = Cate::all();


        return view('admin.product.edit',compact('product','cates'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\ProductEditRequest $request, $id)
    {
        $data = $request->all();
        if ($request->hasFile('image')) {
            $file = $request->file('image');

            $file->move('uploads',$file->getClientOriginalName());
            $data['image'] = $file->getClientOriginalName();
        }

        Product::find($id)->update($data);
        return redirect()->route('admin.product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::find($id)->delete();
        return redirect('admin/product')->with(['msg'=>'xoa thanh cong']);
    }
}

<?php

namespace App\Http\Controllers;
use App\Cate;
use App\Http\Requests\CateAddRequest;
use Illuminate\Http\Request;

class CateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cates = Cate::paginate(10);

        return view('admin.cate.index',compact('cates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cates = Cate::all();
        return view('admin.cate.create',compact('cates'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CateAddRequest $request)
    {
        $data = $request->all();
        $data['alias'] = str_slug($data['title']);
        Cate::create($data);
        return redirect('admin/cate')->with(['msg'=>'them thanh cong']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cates = Cate::where('id','<>',$id)->get();
        $cate = Cate::find($id);
        return view('admin.cate.edit',compact('cates','cate'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data['alias'] = str_slug($data['title']);

        Cate::find($id)->update($data);
        return redirect('admin/cate')->with(['msg'=>'edit thanh cong']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cate::find($id)->delete();
        return redirect('admin/cate')->with(['msg'=>'xoa thanh cong']);

    }
}

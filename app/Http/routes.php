<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix'=>'admin'], function () {
    Route::resource('cate', 'CateController');
    Route::resource('product', 'ProductController');

});
Route::get('', 'HomeController@index');
Route::get('/products', 'HomeController@search')->name('search');
Route::get('{cate}.html', 'HomeController@cate')->name('cate');
Route::get('{cate}/{product}-{id}.html', 'HomeController@detail')->name('detail')->where('product','[\w-]+');
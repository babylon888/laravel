@extends('admin.master')
@section('content')
    <link rel="stylesheet" href="{{asset('adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Quick Example</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" method="post" action="{{route('admin.product.store')}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Title</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter name" name="title">
                    <span class="help-block">{{$errors->first('title')}}</span>

                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Price</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter price" name="price">
                    <span class="help-block">{{$errors->first('price')}}</span>

                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Image</label>
                    <input type="file" class="form-control" id="exampleInputEmail1" placeholder="Enter price" name="image">
                    <span class="help-block">{{$errors->first('image')}}</span>

                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">category</label>
                    <select name="cate_id" id="" class="form-control">
                        @foreach($cates as $cate)
                            <option value="{{$cate->id}}">{{$cate->title}}</option>
                        @endforeach
                    </select>
                    <span class="help-block">{{$errors->first('cate_id')}}</span>

                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Content</label>
                    <textarea name="content" id="editor1" cols="30" rows="10" class="form-control textarea"></textarea>
                    <span class="help-block">{{$errors->first('content')}}</span>

                </div>


            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
    <script src="{{asset('adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
    <script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>
@stop
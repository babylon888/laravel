@extends('admin.master')
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Quick Example</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" method="post" action="{{route('admin.product.update',$product->id)}}">
            {{ csrf_field() }}
            {{method_field('put')}}
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Title</label>
                    <input type="text" value="{{$product->title}}" class="form-control" id="exampleInputEmail1" placeholder="Enter name" name="title">
                    <span class="help-block">{{$errors->first('title')}}</span>

                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Price</label>
                    <input type="text"  value="{{$product->price}}"class="form-control" id="exampleInputEmail1" placeholder="Enter price" name="price">
                    <span class="help-block">{{$errors->first('price')}}</span>

                </div>
                <div class="form-group">
                    <img src="{{asset("uploads/$product->image")}}" alt="" width="100">
                    <label for="exampleInputEmail1">Image</label>
                    <input type="file" class="form-control" id="exampleInputEmail1" placeholder="Enter price" name="image">
                    <span class="help-block">{{$errors->first('image')}}</span>

                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">category</label>
                    <select name="cate_id" id="" class="form-control">
                        @foreach($cates as $value)
                            @if($value->id==$product->cate_id)
                                <option value="{{$value->id}}" selected>{{$value->title}}</option>
                            @else
                                <option value="{{$value->id}}">{{$value->title}}</option>
                            @endif
                        @endforeach
                    </select>
                    <span class="help-block">{{$errors->first('cate_id')}}</span>

                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Content</label>
                    <textarea name="content" id="editor1" cols="30" rows="10" class="form-control textarea">{{$product->content}}</textarea>
                    <span class="help-block">{{$errors->first('content')}}</span>

                </div>


            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
@stop
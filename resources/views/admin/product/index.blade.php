@extends('admin.master')
@section('content')
    <section class="content-header">
        <h1>
            Simple Tables
            <small>preview of simple tables</small>
        </h1>

        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Simple</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">List Products</h3>
                            <a href="{{route('admin.product.create')}}" class="btn btn-success">Add Product</a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered">
                            <tr>
                                <th style="width: 10px">STT</th>
                                <th>Tên</th>
                                <th>image</th>
                                <th>Price</th>
                                <th>category</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                            @forelse($products as $product)
                            <tr>
                                <td>{{$product->id}}</td>
                                <td>{{$product->title}}</td>
                                <td>
                                    <img src="{{asset("uploads/$product->image")}}" alt="" width="100">
                                </td>
                                <td>{{$product->price}}</td>
                                <td>
                                    {{$product->cate->title}}
                                </td>
                                <td>
                                    <a href="{{route('admin.product.edit',$product->id)}}" class="btn btn-primary"
                                    >Edit</a>
                                </td>
                                <td>
                                    <form action="{{route('admin.product.destroy',$product->id)}}" method="post" id="delete{{$product->id}}">
                                        {{ csrf_field() }}
                                        {{method_field('delete')}}
                                    </form>
                                    <a href="#" class="btn btn-danger"
                                       onclick="document.getElementById('delete{{$product->id}}').submit()"
                                    >Delete</a>
                                </td>
                            </tr>
                                @empty
                                <tr>
                                    <td colspan="7">chua co san pham nao</td>
                                </tr>
                            @endforelse



                        </table>
                    </div>
                    <!-- /.box-body -->
                    {{ $products->links() }}

                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->

            <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>

@stop

@extends('admin.master')
@section('content')
    <section class="content-header">
        <h1>
            Simple Tables
            <small>preview of simple tables</small>
        </h1>

        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Simple</li>
        </ol>

    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Category List</h3>
                            <a href="{{route('admin.cate.create')}}" class="btn btn-success">Add Category</a>
                    </div>
                    @if(session('msg'))

                    <div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i> {{session('msg')}}!</h4>
                    </div>
                    @endif
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered table-hover">
                            <tr>
                                <th style="width: 10px">STT</th>
                                <th>Tên</th>
                                <th>Danh mục cha</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                            @forelse($cates as $cate)
                            <tr>
                                <td>{{$cate->id}}</td>
                                <td>{{$cate->title}}</td>
                                <td>
                                        @if($cate->parent_id!=0)
                                            {{\App\Cate::find($cate->parent_id)->title}}
                                         @else
                                            {{'thu muc goc'}}
                                        @endif
                                </td>
                                <td>
                                    <a href="{{route('admin.cate.edit',$cate->id)}}" class="btn btn-primary"
                                    >Edit</a>
                                </td>
                                <td>
                                    <form action="{{route('admin.cate.destroy',$cate->id)}}" method="post" id="delete{{$cate->id}}">
                                        {{ csrf_field() }}
                                        {{method_field('delete')}}
                                    </form>
                                    <a href="#" class="btn btn-danger"
                                    onclick="document.getElementById('delete{{$cate->id}}').submit()"
                                    >Delete</a>
                                </td>
                            </tr>
                                @empty
                                <tr>
                                    <td colspan="5">Chua co danh muc nao</td>
                                </tr>
                            @endforelse



                        </table>
                    </div>
                    <!-- /.box-body -->
                    {{--<div class="box-footer clearfix">
                        <ul class="pagination pagination-sm no-margin pull-right">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">&raquo;</a></li>
                        </ul>
                    </div>--}}
                    {{ $cates->links() }}
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->

            <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>

@stop

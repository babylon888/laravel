@extends('admin.master')
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Quick Example</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" method="post" action="{{route('admin.cate.update',$cate->id)}}">
            {{ csrf_field() }}
            {{method_field('put')}}
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Name</label>
                    <input type="name" value="{{$cate->title}}" class="form-control" id="exampleInputEmail1" placeholder="Enter name" name="title">
                    <span class="help-block">{{$errors->first('title')}}</span>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Parent id</label>
                    <select name="parent_id" id="" class="form-control">
                        <option value="0">Root</option>
                        @foreach($cates as $value)
                            @if($value->id==$cate->parent_id)
                                <option value="{{$value->id}}" selected>{{$value->title}}</option>
                            @else
                                <option value="{{$value->id}}">{{$value->title}}</option>
                            @endif
                        @endforeach
                    </select>
                    <span class="help-block">{{$errors->first('parent_id')}}</span>

                </div>


            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
@stop
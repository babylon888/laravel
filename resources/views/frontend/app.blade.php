<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('theme/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('theme/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('theme/css/bootstrap.css')}}">

    <title>Document</title>
</head>
<body>
<header>
    @include('frontend.menu')
</header>

<section class="content">
@yield('content')
</section>
</body>
</html>
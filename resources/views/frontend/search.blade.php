@extends('frontend.app')
@section('content')
    <h3>Tim kiem theo tu khoa: </h3>
    <div class="row">
        <div class="col-md-1">
            <form action="" method="get" id="search">
            Danh muc
                <input type="hidden" name="name" value="{{isset($_GET['name'])?$_GET['name']:''}}">
                <input type="hidden" name="price" value="{{isset($_GET['price'])?$_GET['price']:''}}">
                <select name="cate_id" id="" class="form-control" onchange="document.getElementById('search').submit()">
                <option value="">vui lòng chọn</option>

                @foreach(\App\Cate::all() as $cate)
                    @if(isset($_GET['cate_id']) && $_GET['cate_id']==$cate->id)
                     <option value="{{$cate->id}}" selected>{{$cate->title}}</option>
                    @else
                        <option value="{{$cate->id}}">{{$cate->title}}</option>
                    @endif

                        @endforeach
            </select>
            </form>

        </div>
        <div class="col-md-4">
            Ten sp:
            <form action="" method="get">
                <input type="text" name="name" value="{{isset($_GET['name'])?$_GET['name']:''}}">
                <input type="hidden" name="cate_id" value="{{isset($_GET['cate_id'])?$_GET['cate_id']:''}}">
                <input type="hidden" name="price" value="{{isset($_GET['price'])?$_GET['price']:''}}">


                <input type="submit" value="Search" class="btn btn-outline-danger">
            </form>
        </div>


        <div class="col-md-4 float-right">
            <form action="" method="get" id="price">
                <input type="hidden" name="name" value="{{isset($_GET['name'])?$_GET['name']:''}}">
                <input type="hidden" name="cate_id" value="{{isset($_GET['cate_id'])?$_GET['cate_id']:''}}">
                Khoang gia:

                <select name="price" id="" class="form-control" onchange="document.getElementById('price').submit()">
                    <option value="">vui lòng chọn </option>

                    <option value="0-1000000"
                    <?php
                        if(isset($_GET['price']) && $_GET['price']=='0-1000000'){
                            echo 'selected';
                        }

                        ?>
                    >0 - 1 trieu</option>
                    <option value="1000000-2000000"
                    <?php
                        if(isset($_GET['price']) && $_GET['price']=='1000000-2000000'){
                            echo 'selected';
                        }

                        ?>
                    >1 trieu - 2 trieu</option>
                </select>
            </form>

        </div>


    </div>

    <div class="row">
        @foreach($products as $product)
            <div class="col-md-3 box">
                <a href="{{route('detail',[$product->cate->alias,str_slug($product->title),$product->id])}}"><img src="{{asset("uploads/$product->image")}}" alt="" ></a>
                <p>{{number_format($product->price,0,',','.')}} vnd</p>
                <p><a href="{{route('detail',[$product->cate->alias,str_slug($product->title),$product->id])}}">{{$product->title}}</a></p>
            </div>
        @endforeach
    </div>
@stop

<ul class="menu">
    <li><a href="{{url('')}}">Home</a></li>
    @foreach($cates as $cate)

        <li><a href="{{route('cate',$cate->alias)}}">{{$cate->title}}</a>

                <?php
                $cate_child = \App\Cate::where('parent_id', $cate->id)->get();
                ?>
                <ul>
                    @foreach($cate_child as $child)
                        <li><a href="{{route('cate',$child->alias)}}">{{$child->title}}</a></li>
                    @endforeach
                </ul>

        </li>
    @endforeach
    <li><a href="{{route('admin.cate.index')}}" target="_blank">Administrator</a></li>
    <li><a href="products">All Products</a></li>

</ul>
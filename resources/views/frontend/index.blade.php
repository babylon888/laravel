@extends('frontend.app')
@section('content')
    <div class="row">
        @foreach($products as $product)
        <div class="col-md-3 box">
            <a href="{{route('detail',[$product->cate->alias,str_slug($product->title),$product->id])}}"><img src="{{asset("uploads/$product->image")}}" alt="" ></a>
            <p>{{number_format($product->price,0,',','.')}} vnd</p>
            <p><a href="{{route('detail',[$product->cate->alias,str_slug($product->title),$product->id])}}">{{$product->title}}</a></p>
        </div>
        @endforeach
    </div>
@stop